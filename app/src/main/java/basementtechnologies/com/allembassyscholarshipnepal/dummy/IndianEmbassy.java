package basementtechnologies.com.allembassyscholarshipnepal.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import basementtechnologies.com.allembassyscholarshipnepal.R;


/**
 * Just dummy content. Nothing special.
 *
 * Created by Andreas Schrade on 14.12.2015.
 */
public class IndianEmbassy {

    /**
     * An array of sample items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<>();

    /**
     * A map of sample items. Key: sample ID; Value: Item.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<>(10);

    static {
        addItem(new DummyItem("1", R.drawable.p1, "COMPEX Scholarship Scheme", "MBBS*,BE,B.Sc (Ag),BVSc & AH,B. Pharmacy,B.Sc (DT),B.Sc (Nursing)", "Under COMPEX Scholarship Scheme, Embassy of India provides admission to undergraduate courses in MBBS(Self financing), Engineering (BE), Pharmacy (B.Pharma), Agriculture (B.Sc.Ag), Dairy Technology (B.Sc. - DT), Veterinary Science & Animal Husbandry (B.V.Sc. & AH) and B.Sc. (Nursing) in Indian Universities and Institutions.","1.MBBS* -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 60% marks in each subject i.e. Physics (P), Chemistry (C) and Biology (B) in Class XII or equivalent.\n\n 2.BE -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 60% marks in each subject i.e. Physics (P), Chemistry (C) and Mathematics (M) in Class XII or equivalent.\n\n 3.B.Sc (Ag) -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 55% marks in each subject i.e. Physics (P), Chemistry (C) and Biology (B) in Class XII or equivalent.\n\n 4.BVSc & AH -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 55% marks in each subject i.e. Physics (P), Chemistry (C) and Biology (B) in Class XII or equivalent.\n\n 5.B. Pharmacy -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 55% marks in each subject i.e. Physics (P), Chemistry (C) and Biology (B) in Class XII or equivalent.\n\n 6.B.Sc (DT) -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 55% marks in each subject i.e. Physics (P), Chemistry (C) and Mathematics (M) in Class XII or equivalent.\n\n 7.B.Sc (Nursing) -60% marks in aggregate and 50% marks in English in Class XII (10+2) and at least 55% marks in each subject i.e. Physics (P), Chemistry (C) and Biology (B) in Class XII or equivalent. \nCandidates appearing in Plus 2 Examination may also apply in anticipation of their results. Their selection will, however, depend on fulfilling the above criteria of eligibility" +
                " \nApplicants should be between 17 to 22 years of age for all undergraduate courses.","Selection will be based on a qualifying examination conducted by the Embassy at Kathmandu and Birgunj.","Scheme is tentatively opens in January-February every year and examination is held in April-May.  Notice of COMPEX Scholarship Scheme is advertised in local newspapers and is uploaded on Embassy of India’s website (www.indianembassy.org.np) nearer the time of opening of this scheme. Eligible interested candidates are requested to go through the instructions and submit the application before the last date as mentioned in the Notice. Please remain in touch with the Embassy’s website for exact date of opening of the scheme."));
        addItem(new DummyItem("2", R.drawable.p1, "AYUSH Scholarship Scheme", "Undergraduate/ Post Graduate courses in Ayurveda/Unani/Homeopathy system", "Under AYUSH scholarship scheme, Embassy of India provides admission to Undergraduate/ Post Graduate courses in Ayurveda/Unani/Homeopathy system of medicines in Indian institutions.","Undergraduate Courses: Candidates must have passed Class-XII or equivalent examination with Physics, Chemistry, Biology and English as subjects and should have secured a minimum of 50% marks in English in class XII. The candidate should be between 17 and 22 years of age. The age restriction may be relaxable in deserving cases, as deemed by the Embassy.\\n\n" +
                "Postgraduate Courses: Candidates must have passed graduation level examination in the recognized system of medicine with a minimum of 50% marks in English in Class-XII examination. The candidate should be 30 years or less.","Selection will be based on academic performances of the candidates. Selected candidates will be notified over phone by Education Wing of the Embassy. Candidates are requested not to contact Embassy for this.","Scheme is tentatively opens in November/December every year.  In this regard, a Notice is advertised in local newspapers and is uploaded on Embassy of India’s website (www.indianembassy.org.np) nearer the time of opening of this scheme. Eligible interested candidates are requested to go through the instructions and submit the application before the last date as mentioned in the Notice. Please remain in touch with the Embassy’s website for exact date of opening of the scheme."));
        addItem(new DummyItem("3", R.drawable.p1, "APS Scholarship Scheme", "Class VI to IX  and XI ", "Under APS Scholarship Scheme, Embassy  of India provides admission to School level courses starting from Class VI to IX  and XI ( boys and girls) in Army Public Schools (APS) in  APS Pithoragarh (Uttarakhand), Class VI to IX (boys only) for APS Dhaula Kuan (Delhi), and class VI to IX (girls only ) for APS Noida (UP). There are 20 seats for Class VI, 5 for Class XI in Pithoragarh, One seat each  from Class VI to IX (Boys only) in Dhaula Kuan and one seat each from Class VI to IX(Girls only) in Noida.","\\n(i) For class VI to  IX   &  XI: 60% marks for Govt school and 70% for private schools.  Students applying for class VI and onwards must have passed the examination before applying. Appearing students will not be considered for the next higher class.\n" +
                "\n (ii) Candidates having good proficiency in English should only apply.\n" +
                "\n (iii) Candidates should be medically fit to join APSs.\n" +
                "\n (iv) Age criteria: The students should complete the under-mentioned years of age:\n" +
                "            \n Class VI:           10 years\n" +
                "            \n Class VII:          11 years\n" +
                "            \n Class VIII:         12 years\n" +
                "            \n Class IX:           13 years\n" +
                "            \n Class XI:           15 years","Selection will be based on interview of the shortlisted candidates. Selected candidates will be notified over phone by Education Wing of the Embassy. Candidates are requested not to contact Embassy for this.","Scheme is tentatively opens in January/February every year.  In this regard, a Notice is advertised in local newspapers and is uploaded on Embassy of India’s website (www.indianembassy.org.np) nearer the time of opening of this scheme. Eligible interested candidates are requested to go through the instructions and submit the application before the last date as mentioned in the Notice. Please remain in touch with the Embassy’s website for exact date of opening of the scheme."));
        addItem(new DummyItem("4", R.drawable.p1, "General Scholarship Scheme (GSS)", "BBA, BCA, BA, BA(Hons), B.Sc., B.Com, BA", "Under GSS, Embassy of India provides admission to under-graduate courses i:e BBA, BCA, BA, BA(Hons), B.Sc., B.Com, BA (Music/Performing Arts) under General Scholarship Scheme in Indian Universities.","","",""));
        addItem(new DummyItem("5", R.drawable.p1, "SAARC Scholarship Scheme", "B.Sc (Hospitality & Hotel Management)", "Under this scheme, admission is provided to B.Sc (Hospitality & Hotel Management) course in Indian institutions.","","",""));
        addItem(new DummyItem("6", R.drawable.p2, "Silver Jubilee Scholarship Scheme (SJSS)", "MBA, MCA, MA, M.Sc & Ph.D ","Under SJSS, admission is provided to Nepalese candidates for MBA, MCA, MA, M.Sc & Ph.D in all streams except Engineering, Agriculture, Medicine and Para-medical courses in Indian Universities and Institutions.","","",""));
        addItem(new DummyItem("7", R.drawable.p3, "Dr. Homi J. Bhabha Scholarship Scheme", "ME", "Under Dr. Homi J. Bhabha Scholarship Scheme, admission is provided to Nepalese candidates in Master of Engineering (ME) course in Indian Universities and Institutions.","","",""));
        addItem(new DummyItem("8", R.drawable.p4, "Nepal Aid Fund Scholarship Scheme ", "M.Sc (Agriculture)","Under Nepal Aid Fund Scholarship Scheme, admission is provided to Nepalese candidates in M.Sc (Agriculture) in Indian Universities and Institutions.","","",""));
        addItem(new DummyItem("9", R.drawable.p5, "ITEC/Colombo Plan", "Short term training ","Under ITEC/Colombo Plan, short term training is provided in diverse fields to employees of GON, public/private sector to upgrade their professional skills. Brochure giving details of courses offered under ITEC/Colombo Plan is available at Embassy of India’s website.","","",""));
        addItem(new DummyItem("10", R.drawable.p5, "MAHATMA GANDHI SCHOLARSHIP SCHEME", "Class-XI","Under Mahatma Gandhi Scholarship Scheme, Embassy of India provides 2000 scholarships to Nepalese students enrolled in Class-XI in recognized schools/colleges in Nepal.","","",""));
        addItem(new DummyItem("11", R.drawable.p5, "GOLDEN JUBILEE SCHOLARSHIP SCHEME", "Bachelor 1styear/1st semester","Under Golden Jubilee Scholarship Scheme, Embassy of India provides scholarships to students enrolled in Bachelor 1styear/1st semester of undergraduate courses in recognized educational institutions in Nepal. A total of 200 scholarships are provided ","","",""));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static class DummyItem {
        public final String id;
        public final int photoId;
        public final String title;
        public final String author;
        public final String content;
        public final String eligibility;
        public final String selectionProcess;
        public final String howToApply;

        public DummyItem(String id, int photoId, String title, String author, String content,String eligibility,String selectionProcess,String howToApply) {
            this.id = id;
            this.photoId = photoId;
            this.title = title;
            this.author = author;
            this.content = content;
            this.eligibility=eligibility;
            this.selectionProcess=selectionProcess;
            this.howToApply=howToApply;
        }
    }
}
