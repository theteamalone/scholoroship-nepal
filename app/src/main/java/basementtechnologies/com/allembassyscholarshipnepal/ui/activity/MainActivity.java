package basementtechnologies.com.allembassyscholarshipnepal.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import basementtechnologies.com.allembassyscholarshipnepal.R;

/**
 * Created by Sujit K on 8/18/2016.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
